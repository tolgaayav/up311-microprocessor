; Example assembly program
.org 0x0000
.equ stack 0xff
.equ size 0x08
; boot code
	movi a, stack
	mov sp, a
	sub d,d,d
	mov e,d
	movi c, size
	jmp _main

.org 0x000F	
; isr0 code 
L:      write @d,e
	inc d
	dec c
	jnz L	
	ret	


_main:  mov h,d
	movi g,0xAA
	write @g,h
	jmp _main
	halt
