#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"

FILE *fp=NULL,*fp2=NULL;
char cmd[1023][5][30];
unsigned  pc[1023], pcnt=0;
unsigned cnt=0,romcnt=0;
unsigned base=0;

enum opcodes{mov,add,sub,and,or,not,inc,dec,sr,sl,rr,jmp,jz,jnz,call,ret,nop,halt,push,pop,write,read,movi,movspr,movrsp};
enum registers{a,b,c,d,e,f,g,h};

union{
 unsigned short int instruction; /* 16-bit instruction */
 struct{ /* R type instruction */
   int u: 2;
   int r3: 3; 
   int r2: 3;
   int r1: 3;
   int opcode: 5;
 }r;
 struct{ /* J type instruction */
   int add: 10;
   int sign: 1;	
   int opcode: 5;
 }j;
 struct{ /* I type instruction */
   int imm8: 8;
   int r1: 3;
   int opcode: 5;
 }i;
}x;

int find_label(char *s,unsigned current)
{
  int i;
  for(i=0; i<cnt; i++) {
    if(!strcmp(cmd[i][0],s)) { 
		    
	return pc[i]-pc[current]-1;
    }
  }
  return 0;
}

int find_num(char *s)
{ 
  int i;
  for(i=0; i<cnt; i++) {
    if(!strcmp(cmd[i][1],".equ")) { 
	    if(!strcmp(cmd[i][2],s)) { 			
		return strtol(cmd[i][3],NULL,16);
	    }
    }
  }
  return strtol(s,NULL,16);
}

void print_binary(unsigned short k, int end, int begin)
{   int i;
    for(i=end-1;i>=begin;i--) printf("%u",(k>>i)&0x1);
}

unsigned int assign_reg(char *s)
{
  if(s[0]=='@'){s[0]=s[1]; s[1]=0;}
  
  if(s[0]=='s') return 0;
  else return s[0]-97;
}

int main(int argc, char **argv)
{
  char s[100],s2[100];
  char  *ch=NULL;
  unsigned char sgn=0;

  int i,j,k,opr=0;
  
  memset(cmd,0,120000);
  memset(s,0,100);memset(s2,0,100);
   
  if(argc<2) {printf("Usage: as311 filename\n"); return 0;}
 
  if((fp=fopen(argv[1],"r"))==NULL) {printf("%s cannot be opened\n",argv[1]); return 0;}

  // FIRST PASS //
  while(fgets(s,100,fp)){

	i=j=opr=0;
	while(*(s+i)!=0){	     
		if(*(s+i)==':'){*(s2+j)=0;j=0; strcpy(cmd[cnt][0],s2); memset(s2,0,100);}	
		else if(j==0 && (*(s+i)==' '||*(s+i)=='\t'||*(s+i)=='\n')){ /*i++; continue;*/}
		else if(cmd[cnt][1][0]==0 && j!=0 && (*(s+i)==' '||*(s+i)==','||*(s+i)=='\n'||*(s+i)=='\t')) 
					{*(s2+j)=0;j=0; strcpy(cmd[cnt][1],s2); memset(s2,0,100);}
		else if(cmd[cnt][1][0]!=0 && j!=0 && (*(s+i)==' '||*(s+i)==','||*(s+i)=='\n')) 
					{*(s2+j)=0;j=0; strcpy(cmd[cnt][2+opr++],s2); if(opr==5)break; memset(s2,0,100);}
		else *(s2+j++)=*(s+i);	
	 i++; 
	}
	if(s[0]=='\n') continue;
	if(cmd[cnt][1][0]!='.' && cmd[cnt][1][0]!=';') pc[cnt]=base+pcnt++;
        if(!strcmp(cmd[cnt][1],".org")) { 
		if(strtol(cmd[cnt][2],NULL,16)>=(base+pcnt)) base=strtol(cmd[cnt][2],NULL,16);
		else { printf("Error in .org directive.\n"); return -1;}
		pcnt=0;
	}
	if(cmd[cnt][1][0]!=0)cnt++;
	memset(s2,0,100);
  }
	
  fclose(fp); 	fp=fopen(strcat(strtok(argv[1],"."),".vhdl_hex"),"w");
		fp2=fopen(strcat(strtok(argv[1],"."),".hex"),"w");
  fprintf(fp,"constant ROM: rom_type :=(\n");

  printf("input line\t\t[address]:machine code\n-------------------\t------------------------- \n");
  
  // SECOND PASS   //	
  for(i=0; i<cnt; i++) {
	x.instruction=0; 
	
     	for(j=0; j<5; j++) if(cmd[i][j][0]!=0) printf("%s ",cmd[i][j]); 
	
	if(!strcmp(cmd[i][1],"mov") && cmd[i][2][0]=='s') {
		x.r.opcode=movspr;x.r.r1=assign_reg(cmd[i][2]);x.r.r2=assign_reg(cmd[i][3]);  }	
	else if(!strcmp(cmd[i][1],"mov") && cmd[i][3][0]=='s') {
		x.r.opcode=movrsp;x.r.r1=assign_reg(cmd[i][2]);x.r.r2=assign_reg(cmd[i][3]);  }	
	else if(!strcmp(cmd[i][1],"mov")) {
		x.r.opcode=mov;x.r.r1=assign_reg(cmd[i][2]);x.r.r2=assign_reg(cmd[i][3]);  }	
	else if(!strcmp(cmd[i][1],"add")) {
		x.r.opcode=add;x.r.r1=assign_reg(cmd[i][2]);x.r.r2=assign_reg(cmd[i][3]);x.r.r3=assign_reg(cmd[i][4]);  }	
	else if(!strcmp(cmd[i][1],"sub")) {
		x.r.opcode=sub;x.r.r1=assign_reg(cmd[i][2]);x.r.r2=assign_reg(cmd[i][3]);x.r.r3=assign_reg(cmd[i][4]);  }	
	else if(!strcmp(cmd[i][1],"and")) {
		x.r.opcode=and;x.r.r1=assign_reg(cmd[i][2]);x.r.r2=assign_reg(cmd[i][3]);x.r.r3=assign_reg(cmd[i][4]);  }	
	else if(!strcmp(cmd[i][1],"or")) {
		x.r.opcode=or;x.r.r1=assign_reg(cmd[i][2]);x.r.r2=assign_reg(cmd[i][3]);x.r.r3=assign_reg(cmd[i][4]);  }	
	else if(!strcmp(cmd[i][1],"not")) {x.r.opcode=not;x.r.r1=assign_reg(cmd[i][2]);x.r.r2=assign_reg(cmd[i][2]); }	
	else if(!strcmp(cmd[i][1],"inc")) {x.r.opcode=inc;x.r.r1=assign_reg(cmd[i][2]);x.r.r2=assign_reg(cmd[i][2]); }	
	else if(!strcmp(cmd[i][1],"dec")) {x.r.opcode=dec;x.r.r1=assign_reg(cmd[i][2]);x.r.r2=assign_reg(cmd[i][2]); }
	else if(!strcmp(cmd[i][1],"sr")) {x.r.opcode=sr;x.r.r1=assign_reg(cmd[i][2]);x.r.r2=assign_reg(cmd[i][2]); }	
	else if(!strcmp(cmd[i][1],"sl")) {x.r.opcode=sl;x.r.r1=assign_reg(cmd[i][2]);x.r.r2=assign_reg(cmd[i][2]);  }	
	else if(!strcmp(cmd[i][1],"rr")) {x.r.opcode=rr;x.r.r1=assign_reg(cmd[i][2]);x.r.r2=assign_reg(cmd[i][2]); }	
	else if(!strcmp(cmd[i][1],"jmp")) {x.j.opcode=jmp;k=find_label(cmd[i][2],i); if(k<0) x.j.sign=1; x.j.add=abs(k); }	
	else if(!strcmp(cmd[i][1],"jz")) {x.j.opcode=jz;k=find_label(cmd[i][2],i); if(k<0) x.j.sign=1; x.j.add=abs(k); }	
	else if(!strcmp(cmd[i][1],"jnz")) {x.j.opcode=jnz;k=find_label(cmd[i][2],i); if(k<0) x.j.sign=1; x.j.add=abs(k);  }	
	else if(!strcmp(cmd[i][1],"call")) {x.j.opcode=call;k=find_label(cmd[i][2],i); if(k<0) x.j.sign=1; x.j.add=abs(k);   }	
	else if(!strcmp(cmd[i][1],"ret")) { x.j.opcode=ret; }	
	else if(!strcmp(cmd[i][1],"nop")) { x.j.opcode=nop; }	
	else if(!strcmp(cmd[i][1],"halt")) {x.j.opcode=halt; }	
	else if(!strcmp(cmd[i][1],"push")) {x.r.opcode=push;x.r.r3=assign_reg(cmd[i][2]);  }	
	else if(!strcmp(cmd[i][1],"pop")) {x.r.opcode=pop;x.r.r1=assign_reg(cmd[i][2]);  }	
	else if(!strcmp(cmd[i][1],"write")) {x.r.opcode=write;x.r.r2=assign_reg(cmd[i][2]);x.r.r3=assign_reg(cmd[i][3]); }	
	else if(!strcmp(cmd[i][1],"read")) {x.r.opcode=read;x.r.r1=assign_reg(cmd[i][2]);x.r.r2=assign_reg(cmd[i][3]); }	
	else if(!strcmp(cmd[i][1],"movi")) {x.i.opcode=movi;x.i.r1=assign_reg(cmd[i][2]);x.i.imm8=find_num(cmd[i][3]); }	

	printf("\r\t\t\t"); 
	if(cmd[i][1][0]!='.' && cmd[i][1][0]!=';') {
		printf("[%u]:\t",pc[i]);
		printf("%s",KRED); print_binary(x.instruction,16,11); 
		printf("%s",KNRM); print_binary(x.instruction,11,0);
	
		if(pc[i]>romcnt) { for(j=0; j<(pc[i]-romcnt); j++){fprintf(fp,"X\"%.4x\",\n",0);fprintf(fp2,"%.4x\n",0); }
					romcnt=pc[i];} 		
		if(pc[i]==romcnt){ 
		    fprintf(fp,"X\"%.4x\", -- ",x.instruction);fprintf(fp2,"%.4x\n",x.instruction);
		    for(j=0; j<5; j++) if(cmd[i][j][0]!=0) fprintf(fp,"%s ",cmd[i][j]); fprintf(fp,"\n");romcnt++;
		}

	}
	printf("\n");
	
  }
  fprintf(fp,"X\"%.4x\");\n",0);
  fprintf(fp,"type rom_type is array(0 to %u) of cell;\n",romcnt);
  return 1;
}
