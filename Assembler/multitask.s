; Scheduling two tasks
.org 0x0000
.equ stack 0xff
.equ size 0x08
; boot code
	movi a, stack
	mov sp, a
	movi d, 0x80
	sl d
	sl d
	sl d
; d=0x0400
	movi e, 0xc8
; interrupt period 200us
	write @d, e
	jmp _main

.org 0x000F	
; isr0 code 
; context switching
	push a
	push b
	push e
	push h
	movi e, 0xc8
	mov h,sp
	inc h
	read a, @h 
	read b, @e 
	write @h, b
	write @e,a
	pop h
	pop e
	pop b
	pop a	
	ret	

_main:  call L 
L:      pop a
	movi b,0x6
	add a,a,b
	write @e,a
	jmp _task2

_task1: sub a,a,a
L1:     inc a
	jmp L1

_task2: sub b,b,b
L2:     dec b
	jmp L2


