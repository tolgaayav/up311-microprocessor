
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity HalfAdder is port(
   a,b:	in std_logic;
   s:	out std_logic;
   c:	out std_logic);
end HalfAdder;
   
architecture Arch of HalfAdder is
begin
    s <= a xor b after 10 ns;
    c <= a and b after 10 ns;
end Arch;
