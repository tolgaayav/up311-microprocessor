library ieee;
use ieee.std_logic_1164.all;

entity testbench is
end testbench;

architecture behavior of testbench is
component FSM is port(
    x, clk: in std_logic;
    y: out std_logic);
end component;
signal input : std_logic;
signal output: std_logic;
signal clock: std_logic;
constant clk_period : time := 1 us;
begin
    fsm1: FSM port map (
        x => input,
        clk => clock,
        y => output
    );

    clk_process :process
        begin
            clock <='0';
            wait for clk_period/2;  --for 0.5 us signal is '0'.
            clock <='1';
            wait for clk_period/2;  --for next 0.5 us signal is '1'.
    end process;

    stim_proc: process
    begin
        input <= '0'; wait for 1.1 us; assert output = '0' report "0/0 failed";
        input <= '1'; wait for 1 us; assert output = '1' report "1/1 failed";
        input <= '0'; wait for 1 us; assert output = '1' report "0/1 failed";
        input <= '1'; wait for 1 us; assert output = '1' report "1/1 failed";
        input <= '0'; wait for 1 us; assert output = '0' report "0/0 failed";
        report "FSM testbench finished";
        wait;
    end process;
end;
