library ieee;
use ieee.std_logic_1164.all;

entity FSM is port(
    x, clk: in std_logic;
    y: out std_logic);
end FSM;

architecture behavior of FSM is
component DFF is
    port (
    d, clk: in std_logic;
    q, qn: out std_logic);
end component;
signal w1, w2, w3, w4, s, sn : std_logic;

begin
    State: DFF port map (
        d => w4,
        clk => clk,
        q => s,
        qn => sn);
    w1 <= not x;
    w2 <= w1 and s;
    w3 <= x and sn;
    w4 <= w2 or w3;
    y <= s or x;
end;
