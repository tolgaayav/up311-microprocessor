library ieee;
use ieee. std_logic_1164.all;
use ieee. std_logic_arith.all;
use ieee. std_logic_unsigned.all;

entity DFF is port(
    d, clk: in std_logic;
    q, qn: out std_logic);
end DFF;

architecture Behavioral of DFF is
begin
    process(clk)
    variable tmp: std_logic := '0';
    begin
        if(clk = '1' and clk'event) then
            if(d='0') then
                tmp:='0';
            elsif(d='1') then
                tmp:='1';
            end if;
        end if;
        q <= tmp;
        qn <= not tmp;
    end process;
end Behavioral;
