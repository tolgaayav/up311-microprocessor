library ieee;
use ieee. std_logic_1164.all;

entity nand3 is port(
    a, b, c: in std_logic;
    o: out std_logic);
end nand3;

architecture arch of nand3 is
begin
	o <= not (a and b and c);
end arch;
