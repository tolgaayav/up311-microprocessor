library ieee;
use ieee. std_logic_1164.all;

entity nand2 is 
generic(
        gate_delay : time := 10 ns   -- 0 needed for synth
    );
port(
    a, b: in std_logic;
    o: out std_logic);
end nand2;

architecture arch of nand2 is
begin
	o <= not (a and b) after gate_delay;
end arch;
