library ieee;
use ieee. std_logic_1164.all;
use ieee. std_logic_arith.all;
use ieee. std_logic_unsigned.all;

entity JK_FF is port(
    j,k,clk: in std_logic;
    q, qn: out std_logic);
end JK_FF;

architecture Behavioral of JK_FF is
begin
    process(clk)
    variable tmp: std_logic;
    begin
        if(clk = '1' and clk'event) then
            if(j='0' and k='0')then
                tmp:=tmp;
            elsif(j='1' and k='1') then
                tmp:= not tmp;
            elsif(j='0' and k='1') then
                tmp:='0';
            else
                tmp:='1';
            end if;
        end if;
        q <= tmp;
        qn <= not tmp;
    end process;
end Behavioral;
