entity nmos is
port (
	p_Out : out std_logic;
	p_In : in std_logic;
	p_Gate: in std_logic
);
end;

architecture Simple of nmos is
begin
	process(p_In, p_Gate)
		variable control: std_Logic;
		begin
		case p_Gate is
			when '0' | 'L' => p_Out <= 'Z';
			when '1' | 'H' => p_Out <= p_In;
			when others => p_Out <= 'X';
		end case;
	end process;
end;
