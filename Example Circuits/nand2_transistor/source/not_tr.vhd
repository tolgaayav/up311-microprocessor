
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity nmos is
generic(
	trans_delay: time := 10 ns
);
port (
	p_Out : out std_logic;
	p_In : in std_logic;
	p_Gate: in std_logic
);
end;

architecture Simple of nmos is
begin
	process(p_In, p_Gate)
		begin
		case p_Gate is
			when '0' | 'L' => p_Out <= 'Z'  after trans_delay;
			when '1' | 'H' => p_Out <= p_In  after trans_delay;
			when others => p_Out <= 'X' after trans_delay;
		end case;
	end process;
end;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.all;

entity pmos is
generic(
	trans_delay: time := 10 ns
);
port (
	p_Out : out std_logic;	
	p_In : in std_logic;	
	p_Gate: in std_logic
);
end;

architecture Simple2 of pmos is
begin
	process(p_In, p_Gate)
		begin
		case p_Gate is
			when '1' | 'H' => p_Out <= 'Z' after trans_delay;
			when '0' | 'L' => p_Out <= p_In after trans_delay;
			when others => p_Out <= 'X' after trans_delay;
		end case;
	end process;
end;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.all;


entity inverter is port(
   a: in std_logic;
   o:	out std_logic);
end inverter;
   
architecture Arch of inverter is

component nmos is port (
	p_Out : out std_logic;
	p_In : in std_logic;
	p_Gate: in std_logic);
end component;
component pmos is port (
	p_Out : out std_logic;
	p_In : in std_logic;
	p_Gate: in std_logic);
end component;
begin

PMOS1: pmos port map(p_Out=>o, p_In=>'1', p_Gate=>a);
NMOS1: nmos port map(p_Out=>o, p_In=>'0', p_Gate=>a);

end Arch;
