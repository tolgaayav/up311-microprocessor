library ieee;
use ieee.std_logic_1164.all;

entity testbench is
end testbench;

architecture behavior of testbench is
    component nand2 is
        port (
            a: in  std_logic;
            b: in  std_logic;
            o: out std_logic);
    end component;
 --   component inverter is port(
 --  a: in std_logic;
 --  o:	out std_logic);
 --  end component;
    signal input: std_logic_vector(1 downto 0);
    signal output: std_logic;
begin
   uut: nand2 port map (
      a => input(0),
      b => input(1),
      o => output );

    --uut: inverter port map(
    --	a=>input,
    --	o=>output
    --);

    stim_proc: process
    begin
        input <= "00"; wait for 100 ns; assert output = '1' report "0+0 failed";
        input <= "01"; wait for 100 ns; assert output = '1' report "0+1 failed";
        input <= "10"; wait for 200 ns; assert output = '1' report "1+0 failed";
        input <= "11"; wait for 200 ns; assert output = '0' report "1+1 failed";
        report "nand2 testbench finished";
       -- input <= '0'; wait for 100 ns; assert output = '1' report "1 failed";
       -- input <= '1'; wait for 200 ns; assert output = '0' report "0 failed";
       -- report "inverter testbench finished";
        wait;
    end process;
end;
