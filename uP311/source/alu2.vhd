-- alu2.vhd: Alternative implementation of ALU

library ieee;
library work;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.uP.all;

entity ALU_Behavioral is port (
     S:                   in std_logic_vector(4 downto 0);
     A, B:                in std_logic_vector(15 downto 0);
     F:                   out std_logic_vector(15 downto 0);
     zero:                out std_logic
);
end ALU_Behavioral;

architecture imp of ALU_Behavioral is
 signal X, Y, ShiftInput:   std_logic_vector(15 downto 0);
 signal c0:                 std_logic;
begin      
   ALU: process(S,A,B)
	begin
	  case S is
		when "00000" => F <= A;
		when "00100" => F <= A and B;
		when "01000" => F <= A or B;
		when "01100" => F <= not A;
		when "10000" => F <= A + B;
		when "10100" => F <= A - B;
		when "11000" => F <= A + 1;
		when "11100" => F <= A - 1;
		when "00001" => F <= to_stdlogicvector(to_bitvector(A) sll 1);
		when "00010" => F <= to_stdlogicvector(to_bitvector(A) srl 1);
		when "00011" => F(15) <= A(0);
				F <= to_stdlogicvector(to_bitvector(A) srl 1);
		when others => F <= "ZZZZZZZZZZZZZZZZ";
	  end case;
	end process;
end imp;



