library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_unsigned.all;

library work;
	use work.uP.all;
	
entity LE16 is port(
	S: in std_logic_vector(2 downto 0);
	A, B: in std_logic_vector(15 downto 0);
	x: out std_logic_vector(15 downto 0));
end LE16;

architecture imp of LE16 is
begin
U0 : LE port map(S,A(0),B(0),x(0));
U1 : LE port map(S,A(1),B(1),x(1));
U2 : LE port map(S,A(2),B(2),x(2));
U3 : LE port map(S,A(3),B(3),x(3));
U4 : LE port map(S,A(4),B(4),x(4));
U5 : LE port map(S,A(5),B(5),x(5));
U6 : LE port map(S,A(6),B(6),x(6));
U7 : LE port map(S,A(7),B(7),x(7));
U8 : LE port map(S,A(8),B(8),x(8));
U9 : LE port map(S,A(9),B(9),x(9));
U10 : LE port map(S,A(10),B(10),x(10));
U11 : LE port map(S,A(11),B(11),x(11));
U12 : LE port map(S,A(12),B(12),x(12));
U13 : LE port map(S,A(13),B(13),x(13));
U14 : LE port map(S,A(14),B(14),x(14));
U15 : LE port map(S,A(15),B(15),x(15));	
end imp;