
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_unsigned.all;
    
entity FA_16 is port(
    A,B : in std_logic_vector(15 downto 0);
    Cin : in std_logic;
    s   : out std_logic_vector(15 downto 0);
    Cout: out std_logic);
end FA_16;

architecture implementation of FA_16 is
component FA port(
    a,b : in std_logic;
    Cin : in std_logic;
    s   : out std_logic;
    Cout: out std_logic);
end component;

signal c : std_logic_vector(15 downto 0);

begin
FA1 : FA port map(A(0),B(0),Cin,s(0),c(0));
FA2 : FA port map(A(1),B(1),c(0),s(1),c(1));
FA3 : FA port map(A(2),B(2),c(1),s(2),c(2));
FA4 : FA port map(A(3),B(3),c(2),s(3),c(3));
FA5 : FA port map(A(4),B(4),c(3),s(4),c(4));
FA6 : FA port map(A(5),B(5),c(4),s(5),c(5));
FA7 : FA port map(A(6),B(6),c(5),s(6),c(6));
FA8 : FA port map(A(7),B(7),c(6),s(7),c(7));
FA9 : FA port map(A(8),B(8),c(7),s(8),c(8));
FA10 : FA port map(A(9),B(9),c(8),s(9),c(9));
FA11 : FA port map(A(10),B(10),c(9),s(10),c(10));
FA12 : FA port map(A(11),B(11),c(10),s(11),c(11));
FA13 : FA port map(A(12),B(12),c(11),s(12),c(12));
FA14 : FA port map(A(13),B(13),c(12),s(13),c(13));
FA15 : FA port map(A(14),B(14),c(13),s(14),c(14));
FA16 : FA port map(A(15),B(15),c(14),s(15),Cout);

end implementation;
