-- timer16.vhd: 16-bit timer

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity timer16 is port(
    addr	: in std_logic_vector(0 downto 0); 
    data	: inout std_logic_vector(15 downto 0);
    cs  	: in std_logic; 
    wr  	: in std_logic; 
    rd  	: in std_logic;
    clk		: in std_logic; -- clock.
    int		: out std_logic; -- interrupt	 
    inta	: in std_logic);
end timer16;

architecture description of timer16 is
subtype cell is std_logic_vector(15 downto 0);
type ram_type is array(0 to 1023) of cell;
signal RF: ram_type;

begin
    process(cs,addr)
    begin
	if (cs='0' and rd='1') then
		data <= RF(conv_integer(addr));		
	elsif (cs='0' and wr='1') then
		RF(conv_integer(addr)) <= data;
	else 
		data <= (others => 'Z');	
	end if;
    end process;

    process(clk,inta)
    begin
        if rising_edge(clk) then
            RF(1) <= RF(1) - 1;
	    if RF(1) = X"0000" then 
		RF(1) <= RF(0); 
		int <= '1';
	    end if;
        end if;

	if (inta='1') then 
		int <= '0'; 
		data <= "ZZZZZZZZZZZZZZ00";
	else
		data <= (others => 'Z');		
	end if;
    end process;
end description;



