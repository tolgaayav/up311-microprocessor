
library ieee;
   use ieee.std_logic_1164.all;
   use ieee.std_logic_unsigned.all;
library work;
    use work.uP.all;

entity FA is port(
   Cin : in std_logic;
   Cout: out std_logic;
   a,b : in std_logic;
   s   : out std_logic);
   
end FA;
   
architecture implementation of FA is
begin
    s <= a xor b xor Cin;
    Cout <= (a and b) or (Cin and (a xor b));
end implementation;