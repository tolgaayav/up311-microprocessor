library ieee;
library work;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_unsigned.all;
    use ieee.numeric_std.all;

    use work.uP.all;
    
entity rom1024 is port(
cs : in std_logic;
oe : in std_logic;
addr : in std_logic_vector (9 downto 0);
data : out std_logic_vector (15 downto 0)
);
end rom1024;

architecture imp of rom1024 is
subtype cell is std_logic_vector(15 downto 0);
type rom_type is array(0 to 4) of cell;

constant ROM: rom_type :=(
X"3000", -- inc a 
X"3120", -- inc b 
X"0a04", -- add c a b 
X"8800", -- halt 
X"0000");


begin
process(cs, oe, addr)
begin
if (cs='0' and oe='1') then
data <= ROM(conv_integer(addr)) after 10 ns; 
else data <= (others=>'Z');
end if;
end process;
end imp;

